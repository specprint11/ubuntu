# Ansible Server

*Server*
192.168.0.111 ubuntu-s01

*Ansible Git Repo*
```
cd ~/git/ubuntu/
```

*Latest Code*
```
git pull
```

# Commands

ansible-playbook playbooks/main.yml --syntax-check

ansible-playbook -i inventory/hosts playbooks/main.yml --ask-become-pass

*run from git root folder*
```
ansible-playbook -i ./2025/inventory/hosts ./2025/playbooks/main.yml -e "ansible_become_pass=$(cat ~/become.txt)"
```

# Project

Status
  + Setup playbook & task 
  + Standard APT Software Install
  + GNOME Settings (gsettings)
    

On Deck
  - Install SAMBA and SMB Config (need to make sure NT1 works on Ubuntu 24!)

    
Backlog    
  - maybe look into dconf for GNOME Settings
  - Setup NFS and Public NFS Share for all systems

