# Welcome to the Ansible Ubuntu Playbook

## Prep Ubuntu

This playbook is built for Ubuntu version 18.x. Before the playbook is ran, the following items should be performed:

- Ubuntu OS (username, computer name, login automatically) 
- sudo apt update
- sudo apt upgrade
- sudo apt install ansible -y
- sudo apt install git -y

Note: These steps can be automated by creating a bash file in the future.

## Running the Playbook

When running this playbook, you must first go to BitBucket, got to the Repo, and go to Repository Settings and uncheck "This is a private repository."

Run the command from terminal: 
  
sudo ansible-pull -U https://SpecPrint@bitbucket.org/specprint11/ubuntu.git


# Software List

The default software each machine should have:
- Ansible
- Git
- FileZilla
- Libre Office (Standard)
- Thunderbird & Firefox (Standard)
- Real VNC Connect (requires copying the package)

# Release Notes

## Roadmap

- Add bash script to to the initial update and install of ansible and git
- add SSH pull and use keys to add computers, allowing the repo to be private
- Automate adding this playbook to a CRON job
- VNC Viewer: The Address Books Configuration File/ Setup


## Version 1.0 

- Updates Apt cache and upgrade system packages
- Added ability to install packages
- Added the RealVNC Connect VNC Viewer package and install automation
- Added Settings for Favorite Apps
- Set default folder view to list
- Disabled Screen Lock on inactivity (requested by management)
- Setup Remote Access
